(function ($) {
    "use strict";

    // change is-checked class on buttons
    $('.button-group').each(function (i, buttonGroup) {
        var $buttonGroup = $(buttonGroup);
        $buttonGroup.on('click', 'a', function () {
            $buttonGroup.find('.is-checked').removeClass('is-checked');
            $(this).addClass('is-checked');
        });
    });

    /* Move Form Fields Label When User Types */
    // for input and textarea fields
    $("input, textarea").keyup(function () {
        if ($(this).val() != '') {
            $(this).addClass('notEmpty');
        } else {
            $(this).removeClass('notEmpty');
        }
    });

    /* Login Form */
    $("#loginForm").validator().on("submit", function (event) {
        if (event.isDefaultPrevented()) {
            cformError();
            csubmitMSG(false, "请填写账号密码");
        } else {
            event.preventDefault();
            csubmitForm();
        }
    });

    function csubmitForm() {
        var body = {
            "username": $.trim($('#login-username').val()),
            "password": $.trim($('#login-password').val())
        };
        var success = function (data, status, xhr) {
            if (status === 'success') {
                try {
                    csubmitMSG(false, data["message"]);
                    if (data["status"] == 200) {
                        setCookie("aud", xhr.getResponseHeader("Audience"))
                        setCookie("auth", xhr.getResponseHeader("Authorization"))
                        setCookie("uname", data["data"]["uname"])
                        setCookie("avatar", data["data"]["avatar"])
                        setCookie("ugroup", data["data"]["ugroup"])
                        setCookie("uscc", data["data"]["uscc"])
                        location.href = "http://8.141.54.216:12003";
                        return;
                    }
                } catch (e) {
                    console.log(e);
                }
            } else {
                csubmitMSG(false, "网络错误！");
            }
            cformSuccess();
        };
        try {
            $("#btn_submit").attr("disabled", true);
            setInterval(function () {
                csubmitMSG(false, "连接超时！");
                $("#btn_submit").attr("disabled", false);
            }, 5000);
            fc("/login", 'POST', body, success, cformError(), cformComplete());
        } catch (e) {
            console.log(e);
        }
        return false;
    }

    function cformSuccess() {
        $("#btn_submit").attr("disabled", false);
        $("#loginForm")[0].reset();
        $("input").removeClass('notEmpty'); // resets the field label after submission
        $("textarea").removeClass('notEmpty'); // resets the field label after submission
    }

    function cformError() {
        $("#loginForm").removeClass().addClass('shake animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
            $(this).removeClass();
        });
    }

    function cformComplete() {
    }

    function csubmitMSG(valid, msg) {
        if (valid) {
            var msgClasses = "h3 text-center tada animated";
        } else {
            var msgClasses = "h3 text-center";
        }
        $("#cmsgSubmit").removeClass().addClass(msgClasses).text(msg);
    }


    /* Back To Top Button */
    // create the back to top button
    $('body').prepend('<a href="body" class="back-to-top page-scroll">Back to Top</a>');
    var amountScrolled = 700;
    $(window).scroll(function () {
        if ($(window).scrollTop() > amountScrolled) {
            $('a.back-to-top').fadeIn('500');
        } else {
            $('a.back-to-top').fadeOut('500');
        }
    });


    /* Removes Long Focus On Buttons */
    $(".button, a, button").mouseup(function () {
        $(this).blur();
    });

})(jQuery);